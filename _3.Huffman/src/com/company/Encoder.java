package com.company;

import java.util.PriorityQueue;

public class Encoder {
    private static final int BITS_PER_BYTE = 8;
    private static final int ALPHABET_SIZE = 256;

    private static TreeNode createHuffmanTree(long[] freq) {
        PriorityQueue<TreeNode> pq = new PriorityQueue<>(ALPHABET_SIZE);
        TreeNode a, b;

        for (int i = 0; i < ALPHABET_SIZE; ++i)
            if (freq[i] > 0)
                pq.add(new LeafNode(i, freq[i]));

        while (pq.size() > 1) {
            a = pq.poll();
            b = pq.poll();
            pq.add(TreeNode.unify(a, b));
        }

        return pq.peek();
    }

    private static void passTreeAndCollectCodes(TreeNode node, BitStruct bs, BitStruct[] codes) {
        if (node == null)
            return;
        if (node.getLeft() == null && node.getRight() == null) {
            codes[((LeafNode)node).getKey()] = new BitStruct(bs);
        }
        else {
            bs.add(false);
            passTreeAndCollectCodes(node.getLeft(), bs, codes);
            bs.add(true);
            passTreeAndCollectCodes(node.getRight(), bs, codes);
        }
        bs.remove(bs.size() - 1, 1);
    }

    /*private static void pass(TreeNode node) {
        if (node == null)
            return;
        System.out.format("(");
        pass(node.getLeft());
        if (node.getLeft() == null &&  node.getRight() == null)
            System.out.format("%c", (byte)((LeafNode)node).getKey());
        else
            System.out.format("%d", node.getWeight());
        pass(node.getRight());
        System.out.format(")");
    }*/

    private static long[] getFrequencies(String ifname) {
        BytesReader br = new BytesReader(ifname);
        long[] freq = new long[ALPHABET_SIZE];
        int key;
        while ((key = br.read()) != -1)
            ++freq[key];
        return freq;
    }

    private static int singleDistinctByte(final long[] freq) {
        int nBytes = 0, lastByte = 0;
        for (int i = 0; i < freq.length; ++i)
            if (freq[i] > 0) {
                lastByte = i;
                ++nBytes;
            }
        if (nBytes == 1)
            return lastByte;
        return -1;
    }

    private static BitStruct[] createCodes(final long[] freq) {
        BitStruct[] codes = new BitStruct[ALPHABET_SIZE];
        BitStruct bs = new BitStruct();
        TreeNode root;
        int b;

        if ((b = singleDistinctByte(freq)) != -1) {
            codes[b] = new BitStruct();
            codes[b].add(true);
        }
        else {
            root = createHuffmanTree(freq);
            passTreeAndCollectCodes(root, bs, codes);
        }

        return codes;
    }

    private static void serializeCodes(BytesWriter bw, BitStruct[] codes) {
        Byte[] ba;
        for (int i = 0; i < codes.length; ++i)
            if (codes[i] != null) {
                bw.write(i);
                bw.write(codes[i].size());
                ba = codes[i].ToByteArray();
                for (int j = 0; j < ba.length; ++j)
                    bw.write(ba[j]);
                System.out.format("%4d -> %s (%d)\n", (byte)i, codes[i], codes[i].size());
            }
        for (int i = 0; i < 2; ++i)
            bw.write(0);
    }

    private static int lastByteValidBits(final long[] freq, final BitStruct[] codes) {
        int nbits = 0;
        for (int i = 0; i < codes.length; ++i)
            if (codes[i] != null)
                nbits = (nbits + ((int)(freq[i] % BITS_PER_BYTE) * (codes[i].size()) % BITS_PER_BYTE)) % BITS_PER_BYTE;
        return nbits;
    }

    private static void statistic(long[] freq, BitStruct[] codes) {
        long messageLength = 0;
        int alphabetSize = 0, minCodeLength = ALPHABET_SIZE, maxCodeLength = 0,
            codeLength;
        for (int i = 0; i < freq.length; ++i)
            if (freq[i] > 0) {
                codeLength = codes[i].size();
                messageLength += freq[i] * codeLength;
                ++alphabetSize;
                minCodeLength = Math.min(minCodeLength, codes[i].size());
                maxCodeLength = Math.max(maxCodeLength, codes[i].size());
            }
        messageLength /= BITS_PER_BYTE;

        System.out.format("Size of message == %d bytes\n", messageLength);
        System.out.format("Alphabet's size == %d\n", alphabetSize);
        System.out.format("Minimal code's length == %d bits\n", minCodeLength);
        System.out.format("Maximum code's length == %d bits\n", maxCodeLength);
    }

    public static void encode(String ifname, String ofname) {
        BytesReader br;
        BytesWriter bw;
        BitStruct bs = new BitStruct(BITS_PER_BYTE);
        BitStruct[] codes;
        Byte[] ba;
        long[] freq;
        int c, nbytes, nbits;

        freq = getFrequencies(ifname);
        codes = createCodes(freq);

        nbits = lastByteValidBits(freq, codes);

        System.out.format("Number of valid bits in last byte == %d\n", nbits);

        bw = new BytesWriter(ofname);
        bw.write(nbits);
        serializeCodes(bw, codes);

        br = new BytesReader(ifname);
        while ((c = br.read()) != -1) {
            bs.add(codes[c]);
            nbytes = bs.size() / BITS_PER_BYTE;
            if (nbytes > 0) {
                ba = bs.ToByteArray();
                for (int i = 0; i < nbytes; ++i)
                    bw.write(ba[i]);
                bs.remove(0, BITS_PER_BYTE * nbytes);
            }
        }
        if (!bs.isEmpty())
            bw.write(bs.ToByteArray()[0]);

        // Не влияет на работу основного алгоритма
        statistic(freq, codes);

        br.close();
        bw.close();
    }
}
