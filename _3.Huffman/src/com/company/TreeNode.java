package com.company;

public class TreeNode implements Comparable<TreeNode> {
    private TreeNode left, right;
    private long weight;

    TreeNode(long weight) {
        this.weight = weight;
        left = right = null;
    }

    TreeNode(long weight, TreeNode left, TreeNode right) {
        this.weight = weight;
        this.left = left;
        this.right = right;
    }

    public TreeNode getLeft() {
        return left;
    }

    public TreeNode getRight() {
        return right;
    }

    public void setLeft(TreeNode left_) {
        left = left_;
    }

    public void setRight(TreeNode right_) {
        right = right_;
    }

    public long getWeight() {
        return weight;
    }

    public static TreeNode unify(TreeNode a, TreeNode b) {
        return new TreeNode(a.getWeight() + b.getWeight(), a, b);
    }

    public int compareTo(TreeNode other) {
        if (getWeight() < other.getWeight())
            return -1;
        if (getWeight() > other.getWeight())
            return 1;
        return 0;
    }
}
