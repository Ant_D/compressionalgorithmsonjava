package com.company;

public class Decoder {
    private static final int ALPHABET_SIZE = 256;
    private static final int BITS_PER_BYTE = 8;

    private static TreeNode createHuffmanTree(BitStruct[] codes) {
        TreeNode root = new TreeNode(0, null, null),
                cur;
        int length;
        for (int key = 0; key < ALPHABET_SIZE; ++key) {
            if (codes[key] == null)
                continue;
            cur = root;
            length = codes[key].size();
            for (int i = 0; i < length - 1; ++i) {
                if (codes[key].get(i) == false) {
                    if (cur.getLeft() == null)
                        cur.setLeft(new TreeNode(0, null, null));
                    cur = cur.getLeft();
                }
                else {
                    if (cur.getRight() == null)
                        cur.setRight(new TreeNode(0, null, null));
                    cur = cur.getRight();
                }
            }
            if (codes[key].get(length - 1) == false)
                cur.setLeft(new LeafNode(key, 0));
            else
                cur.setRight(new LeafNode(key, 0));
        }
        return root;
    }

    private static BitStruct[] unserializeCodes(BytesReader br) {
        BitStruct[] codes = new BitStruct[ALPHABET_SIZE];
        int key, nbits, nbytes, c;

        while (true) {
            key = br.read();
            nbits = br.read();
            if (key == 0 && nbits == 0)
                break;

            codes[key] = new BitStruct(nbits);
            nbytes = (nbits + BITS_PER_BYTE - 1) / BITS_PER_BYTE;
            for (int j = 0; j < nbytes; ++j) {
                c = br.read();
                codes[key].add((byte)c);
            }
            codes[key].remove(nbits, codes[key].size());
        }

        for (int i = 0; i < codes.length; ++i)
            if (codes[i] != null)
                System.out.format("%4d -> %s (%d)\n", (byte)i, codes[i], codes[i].size());

        return codes;
    }

    public static void decode(String ifname, String ofname) {
        BytesReader br = new BytesReader(ifname);
        BytesWriter bw = new BytesWriter(ofname);
        BitStruct[] codes;
        BitStruct bs = new BitStruct();
        TreeNode root, cur;
        int b, nLastByteValidBits, nbits;
        boolean bit;

        nLastByteValidBits = br.read();
        codes = unserializeCodes(br);
        root = createHuffmanTree(codes);

        b = br.read();
        cur = root;
        while (b != -1) {
            bs.clear();
            bs.add((byte)b);

            b = br.read();
            if (b != -1 || nLastByteValidBits == 0)
                nbits = bs.size();
            else
                nbits = nLastByteValidBits;

            for (int i = 0; i < nbits; ++i) {
                bit = bs.get(i);
                if (bit == false)
                    cur = cur.getLeft();
                else
                    cur = cur.getRight();
                if (cur.getLeft() == null && cur.getRight() == null) {
                    bw.write(((LeafNode)cur).getKey());
                    cur = root;
                }
            }
        }

        br.close();
        bw.close();
    }

}
