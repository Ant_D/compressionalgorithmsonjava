package com.company;

public class LeafNode extends TreeNode {
    private int key;

    LeafNode(int key, long weight) {
        super(weight);
        this.key = key;
    }

    public int getKey() {
        return key;
    }
}