package com.company;

import java.io.OutputStream;
import java.io.FileOutputStream;

public class BytesWriter{
    private static final int BUFFER_SIZE = 4096;

    private int _pos;
    private byte[] _buffer;
    private OutputStream _ostream;

    BytesWriter() {
        _pos = 0;
        _buffer = new byte[BUFFER_SIZE];
    }

    BytesWriter(String fname) {
        this();
        try {
            _ostream = new FileOutputStream(fname);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            System.exit(0);
        }
    }

    BytesWriter(OutputStream ostream) {
        this();
        _ostream = ostream;
    }

    protected void finalize() {
        close();
    }

    public void close() {
        flush();
        try {
            _ostream.close();
        }
        catch (Exception ex) {
            ex.printStackTrace();
            System.exit(0);
        }
    }

    public void flush() {
        try {
            _ostream.write(_buffer, 0, _pos);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            System.exit(0);
        }
        _pos = 0;
    }

    public void write(int b) {
        _buffer[_pos++] = (byte)b;
        if (_pos == BUFFER_SIZE)
            flush();
    }
}