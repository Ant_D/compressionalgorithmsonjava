#include <cstdio>
#include <cstdlib>

const int MAX_NUM = 1024;

int main(int argv, char **argc) {
	if (argv != 3) {
		printf("Provide two parameters: output file and number of repeating of each byte");
		return 0;
	}
	
	FILE *out = fopen(argc[1], "w");
	
	if (out == NULL) {
		printf("I can't open file %s", argc[1]);
		return 0;
	}
	
	int num = (int)strtol(argc[2], NULL, 10);
	
	if (!(0 < num && num <= MAX_NUM)) {
		printf("Number of repeating should be in range (0; %d)", MAX_NUM);
		return 0;
	}
	
	for (int i = -128; i < 128; ++i)
		for (int j = 0; j < num; ++j)
			fprintf(out, "%c", (char)i);
	
	fclose(out);
	
	return 0;
}
