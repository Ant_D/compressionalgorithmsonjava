#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#define FIB_NUM 300

int main(int argc, char **argv) {
	if (argc != 3) {
		printf("Required two parameters: file name and size of alphabet.");
		return EXIT_FAILURE;
	}

	FILE *out;
	int f[FIB_NUM] = {0, 1, 1}, nFib, i, j;
	
	out = fopen(argv[1], "w");
	if (out == NULL) {
		printf("Error: file opening.");
		return EXIT_FAILURE;
	}
	
	nFib = strtol(argv[2], NULL, 10);
	if (errno) {
		printf("Error: string to long.");
		return EXIT_FAILURE;
	}
	
	if (!(0 < nFib && nFib <= 256)) {
		printf("Size of alphabet must be between 1 and 256");
		return EXIT_FAILURE;
	}
	
	for (i = 3; i <= nFib; ++i)
		f[i] = f[i - 1] + f[i - 2];
		
	for (i = 1; i <= nFib; ++i)
		for (j = 0; j < f[i]; ++j)
			fprintf(out, "%c", 'a' + i - 1);
	
	fclose(out);
	
	return 0;
}
