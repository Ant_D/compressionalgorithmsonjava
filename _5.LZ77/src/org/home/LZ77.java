package org.home;

import javafx.util.Pair;

import java.io.InputStream;
import java.io.OutputStream;

public class LZ77 {
    private static final int WINDOW_SZ = 256, // Максимум должен быть 256
            PATTERN_SZ = 256, // Максимум должен быть 256
            BUFFER_SZ = 256; // Должно быть >= 3
	
	// Сдвигает содержимое массива str на shift ячеек влево
    private static int shiftToLeft(int strSz, byte[] str, int shift) {
        for (int i = 0; i + shift < strSz; ++i) {
            str[i] = str[i + shift];
        }
        return strSz - shift;
    }

	// Добавляет в конец массива to nbytes элементов из начала массива from.
	// При необходимости сдвигает данные массива to влево.
    private static int pushBack(int toSz, byte[] to, int nbytes, byte[] from) {
        if (nbytes <= 0) {
            return toSz;
        }
        if (to.length - toSz < nbytes) {
            toSz = shiftToLeft(toSz, to, nbytes + toSz - to.length);
        }
        for (int i = 0; i < nbytes; ++i) {
            to[toSz + i] = from[i];
        }
        toSz += nbytes;
        return toSz;
    }

	// Добавляет в конец массива to элемент from.
	// При необходимости сдвигает данные массива to влево.
    private static int pushBack(int toSz, byte[] to, byte from) {
        if (to.length - toSz < 1) {
            toSz = shiftToLeft(toSz, to, 1 + toSz - to.length);
        }
        to[toSz++] = from;
        return toSz;
    }

	// Отладочный вывод массива b
    private static void printArray(int sz, byte[] b, String prefix) {
        System.out.format("%s", prefix);
        for (int i = 0; i < sz; ++i) {
            System.out.format("%c ", (char)b[i]);
        }
        System.out.println();
    }

	// [1; 127] -> [1; 127], [-128; 0] -> [128; 256]
    private static int byteToInt(byte b) {
        if (b <= 0)
            return (int)b + 256;
        return b;
    }

	// Кодирует данные из istream и записывает их в ostream
    public static void encode(InputStream istream, OutputStream ostream) {
        BytesReader br = new BytesReader(istream);
        BytesWriter bw = new BytesWriter(ostream);
        BitStruct bs = new BitStruct();
        byte[] window = new byte[WINDOW_SZ],
                pattern = new byte[PATTERN_SZ];
        int length = 0, offset = 0,
                windowSz = 0, patternSz = 0, nbytes;
        long before = 0, after = 0;
        Pair<Integer, Integer> occurence;

        nbytes = br.read(pattern, 0, PATTERN_SZ);
        for (patternSz = nbytes; patternSz > 0; patternSz += nbytes) {
            before += nbytes * 8;
//            printArray(windowSz, window, "window: ");
//            printArray(patternSz, pattern, "pattern: ");

            // Нахождение наибольшего вхождения образца в окне
            occurence = PatternSearcher.findLongestOccurence(patternSz, pattern, windowSz, window);
            length = occurence.getKey();
            offset = occurence.getValue();

            // Кодирование
            if (length <= 1) {
                length = 1;
                bs.add(false);
                bs.add(pattern[0]);
//                System.out.println((char)pattern[0]);
            }
            else {
                bs.add(true);
                bs.add((byte)offset);
                bs.add((byte)length);
//                System.out.format("%d %d\n", offset, length);
            }
            while (bs.getnbytes() > 1) {
                after += 8;
                bw.write(bs.popByte());
            }

            // Сдвиги
            windowSz = pushBack(windowSz, window, length, pattern);
            patternSz = shiftToLeft(patternSz, pattern, length);

            // Чтение
            nbytes = br.read(pattern, patternSz, length);
            nbytes = (nbytes < 0) ? 0 : nbytes;
        }

        bs.add(true);
        after += bs.size();
        while (bs.getnbytes() > 0) {
            bw.write(bs.popByte());
        }

        System.out.format("%d bits are compressed to %d bits\n", before, after);

        br.close();
        bw.close();
    }

	// Декодирует данные из istream и записывает их в ostream
    public static void decode(InputStream istream, OutputStream ostream) {
        BytesReader br = new BytesReader(istream);
        BytesWriter bw = new BytesWriter(ostream);
        byte[] window = new byte[WINDOW_SZ],
                buffer = new byte[BUFFER_SZ],
                windowCopy = new byte[WINDOW_SZ];
        int bufferSz = 0, windowSz = 0, length, offset;
        BitStruct bs = new BitStruct();
        byte b;

        while (bufferSz >= 0) {
//            printArray(windowSz, window, "window: ");
            bufferSz = br.read(buffer, 0, buffer.length);
            for (int i = 0; i < bufferSz; ++i) {
                bs.add(buffer[i]);
            }
            while (bs.getnbytes() >= 3 || bufferSz < 0) {
                if (bs.popBit() == false) {
                    b = bs.popByte();
                    bw.write(b);
                    windowSz = pushBack(windowSz, window, b);
                }
                else {
                    if (bs.getnbytes() < 2) {
                        break;
                    }
                    offset = byteToInt(bs.popByte());
                    length = byteToInt(bs.popByte());
//                    System.out.format("offset == %d, length == %d\n", offset, length);
                    for (int i = 0; i < length; ++i) {
                        windowCopy[i] = window[windowSz - offset + i];
                    }
                    bw.write(windowCopy, 0, length);
                    windowSz = pushBack(windowSz, window, length, windowCopy);
                }
            }
        }

        br.close();
        bw.close();
    }
}