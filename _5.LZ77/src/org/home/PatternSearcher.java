package org.home;

import javafx.util.Pair;

public class PatternSearcher {
    private static int[] tZ = new int[0], ptZ = new int[0];

	// Z-функция для образца
    private static void zetFunc(int sz, byte[] b, int[] z) {
        int l = 0, r = 0;
        z[0] = 0;
        for (int i = 1; i < sz; ++i) {
            z[i] = 0;
            if (i <= r) {
                z[i] = Math.min(r - i + 1, z[i - l]);
            }
            while (i + z[i] < sz && b[z[i]] == b[i + z[i]]) {
                ++z[i];
            }
            if (i + z[i] - 1 > r) {
                l = i;
                r = i + z[i] - 1;
            }
        }
    }

	// Z-функция для текста, где ищется образец
    private static void zetFuncWithPattern(int tSz, byte[] t, int[] tZ, int ptSz, byte[] pt, int[] ptZ) {
        int l = 0, r = 0;
        for (int i = 0; i < tSz; ++i) {
            tZ[i] = 0;
            if (i <= r) {
                tZ[i] = Math.min(r - i + 1, ptZ[i - l]);
            }
            while (tZ[i] < ptSz && i + tZ[i] < tSz && pt[tZ[i]] == t[i + tZ[i]]) {
                ++tZ[i];
            }
            if (i + tZ[i] - 1 > r) {
                l = i;
                r = i + tZ[i] - 1;
            }
        }
    }

	// Находит наибольшее вхождение pattern в text
	// Возвращает пару <длина наибольшего вхождения, позиция относительно конца>
    public static Pair<Integer, Integer> findLongestOccurence(int patternSz, byte[] pattern, int textSz, byte[] text) {
        if (tZ.length != textSz) {
            tZ = new int[textSz];
        }
        if (ptZ.length != patternSz) {
            ptZ = new int[patternSz];
        }
        zetFunc(patternSz, pattern, ptZ);
        zetFuncWithPattern(textSz, text, tZ, patternSz, pattern, ptZ);

        int length = 0, offset = 0;
        for (int i = 0; i < textSz; ++i) {
            if (tZ[i] > length) {
                length = tZ[i];
                offset = textSz - i;
            }
        }

        return new Pair<Integer, Integer>(length, offset);
    }
}
