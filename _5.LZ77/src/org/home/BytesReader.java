package org.home;

import com.sun.istack.internal.NotNull;

import java.io.InputStream;
import java.io.FileInputStream;

public class BytesReader {
    private static final int BUFFER_SIZE = 4096;

    private int _pos, _nBytes;
    private byte[] _buffer;
    private InputStream _istream;

    private int byteToInt(byte b) {
        if (b < 0)
            return (int)b + 256;
        return b;
    }

    @Override
    protected void finalize() {
        close();
    }

    BytesReader() {
        _pos = 0;
        _nBytes = 0;
        _buffer = new byte[BUFFER_SIZE];
    }

    BytesReader(String fname) {
        this();
        try {
            _istream = new FileInputStream(fname);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            System.exit(0);
        }
    }

    BytesReader(InputStream istream) {
        this();
        _istream = istream;
    }

    public void close() {
        try {
            _istream.close();
        }
        catch (Exception ex) {
            ex.printStackTrace();
            System.exit(0);
        }
    }

    public int read() {
        if (_pos >= _nBytes) {
            try {
                _nBytes = _istream.read(_buffer);
            }
            catch (Exception ex) {
                ex.printStackTrace();
                System.exit(0);
            }
            _pos = 0;
        }

        if (_nBytes == -1)
            return -1;

        return byteToInt(_buffer[_pos++]);
    }

    public int read(@NotNull byte[] b, int offset, int length) {
        int c;
        for (int i = offset; i < offset + length; ++i) {
            c = read();
            if (c == -1)
                if (i != offset)
                    return i - offset;
                else
                    return -1;
            b[i] = (byte)c;
        }
        return length;
    }
}