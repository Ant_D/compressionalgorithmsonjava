package org.home;

import java.util.ArrayList;

public class BitStruct {
    private final byte BITS_PER_BYTE = 8;
    private int nbits;
    private ArrayList<Byte> storage;

    private int getnbytes(int nbits) {
        return (nbits + BITS_PER_BYTE - 1) / BITS_PER_BYTE;
    }

    private int getByteIndex(int index) {
        return index / BITS_PER_BYTE;
    }

    private int getBitIndex(int index) {
        return index % BITS_PER_BYTE;
    }

    private boolean getBitFromByte(byte b, int bitIndex) {
        return ((b & (1 << bitIndex)) != 0);
    }

    BitStruct() {
        nbits = 0;
        storage = new ArrayList<>();
    }

    BitStruct(int initialCapacityInBits) {
        this();
        storage.ensureCapacity(initialCapacityInBits);
    }

    BitStruct(final BitStruct other) {
        this(0);
        add(other);
    }

    public int size() {
        return nbits;
    }

    public int getnbytes() {
        return (nbits + BITS_PER_BYTE - 1) / BITS_PER_BYTE;
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public void clear() {
        nbits = 0;
        storage.clear();
    }

    public void set(int index, boolean value) {
        int byteIndex = getByteIndex(index),
                bitIndex = getBitIndex(index);
        byte b = storage.get(byteIndex);
        b &= ~(1 << bitIndex);
        b |= ((value) ? 1 : 0) << bitIndex;
        storage.set(byteIndex, b);
    }

    public boolean get(int index) {
        int byteIndex = getByteIndex(index),
                bitIndex = getBitIndex(index);
        byte b = storage.get(byteIndex);
        return getBitFromByte(b, bitIndex);
    }

    public void add(boolean value) {
        if (getnbytes(size() + 1) > storage.size())
            storage.add((byte)0);
        set(nbits, value);
        ++nbits;
    }

    public void add(byte b) {
        for (int i = 0; i < BITS_PER_BYTE; ++i)
            add(getBitFromByte(b, i));
    }

    public void add(BitStruct other) {
        for (int i = 0; i < other.size(); ++i)
            this.add(other.get(i));
    }

    public void remove(int index, int length) {
        int i;
        for (i = index; i + length < size(); ++i)
            set(i, get(i + length));
        nbits = i;
    }

    public Byte[] ToByteArray() {
        Byte[] tmp = new Byte[0];
        return storage.toArray(tmp);
    }

    public byte[] popCompleteBytes() {
        int maxnbytes = getnbytes(size()),
                nbytes = size() / BITS_PER_BYTE;
        byte[] ans = new byte[nbytes];
        for (int i = 0; i < nbytes; ++i) {
            ans[i] = storage.get(i);
        }
        remove(0, nbytes * BITS_PER_BYTE);
        return ans;
    }

    public boolean popBit() {
        boolean ans = get(0);
        remove(0, 1);
        return ans;
    }

    public byte popByte() {
        byte ans = storage.get(0);
        remove(0, BITS_PER_BYTE);
        return ans;
    }

    @Override
    public String toString() {
        String str = "";
        for (int i = 0; i < nbits; ++i)
            str += (get(i)) ? "1" : "0";
        return str;
    }
}