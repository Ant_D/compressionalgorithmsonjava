import java.io.*;

public class FileComparator {
    public static final int DIFF_NBYTES = 16;

    public static void main(String args[]){
        long start = System.currentTimeMillis();
        if (args.length != 2) {
            System.out.println("Two file names are required.");
            return;
        }

        int pos = 0, nb1, nb2;
        byte[] b1 = new byte[1], b2 = new byte[1];
        BufferedInputStream bis1, bis2;

        try {
            bis1 = new BufferedInputStream(new FileInputStream(args[0]));
            bis2 = new BufferedInputStream(new FileInputStream(args[1]));

            do {
                ++pos;
                nb1 = bis1.read(b1);
                nb2 = bis2.read(b2);
            }
            while (nb1 > 0 && nb2 > 0 && b1[0] == b2[0]);

            if (nb1 > 0 && nb2 <= 0 || nb2 > 0 && nb1 <= 0 || nb1 > 0 && nb2 > 0 && b1[0] != b2[0]) {
                System.out.format("Difference at position %x\n", pos);
                System.out.println("*** \"" + args[0] + "\"");
                _printNBytes(nb1, b1, bis1, DIFF_NBYTES);
                System.out.println("*** \"" + args[1] + "\"");
                _printNBytes(nb2, b2, bis2, DIFF_NBYTES);
            }
            else {
                System.out.println("Files \"" + args[0] + "\", \"" + args[1] + "\" are equaled.");
            }

            bis1.close();
            bis2.close();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        System.out.format("Time: %d ms\n", System.currentTimeMillis() - start);
    }

    private static void _printNBytes(int nb, byte[] b, BufferedInputStream bis, int nBytes) {
        try {
            for (int i = 0; i < nBytes && nb > 0; ++i) {
                System.out.format("%x ", b[0]);
                nb = bis.read(b);
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        System.out.println();
    }

}