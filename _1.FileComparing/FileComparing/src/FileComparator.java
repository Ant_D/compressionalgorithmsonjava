import java.io.*;

public class FileComparator {
    public static final int DIFF_NBYTES = 16;

    public static void main(String args[]){
        long start = System.currentTimeMillis();
        if (args.length != 2) {
            System.out.println("Two file names are required.");
            return;
        }

        int pos = 0;
        BytesReader reader1 = new BytesReader(args[0]),
                    reader2 = new BytesReader(args[1]);

        while (!reader1.eof() && !reader2.eof() && reader1.current() == reader2.current()) {
            reader1.next();
            reader2.next();
            ++pos;
        }

        if (!reader1.eof() || !reader2.eof() || reader1.current() !=  reader2.current()) {
            System.out.format("Difference at position %x\n", pos);
            System.out.println("*** \"" + args[0] + "\"");
            reader1.printNBytes(DIFF_NBYTES);
            System.out.println("*** \"" + args[1] + "\"");
            reader2.printNBytes(DIFF_NBYTES);
        }
        else {
            System.out.println("Files \"" + args[0] + "\", \"" + args[1] + "\" are equaled.");
        }

        System.out.format("Time: %d ms\n", System.currentTimeMillis() - start);
    }

}