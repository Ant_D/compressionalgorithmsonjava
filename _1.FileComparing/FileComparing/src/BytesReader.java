import java.io.FileInputStream;

public class BytesReader {
    public static final int BUFFER_SIZE = 4096;

    private int _pos, _nBytes;
    private byte[] _buffer;
    private FileInputStream _file;

    BytesReader(String fname) {
        _buffer = new byte[BUFFER_SIZE];
        try {
            _file = new FileInputStream(fname);
            _nBytes = _file.read(_buffer);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            System.exit(0);
        }
        _pos = 0;
    }

    protected void finalize() {
        try {
            _file.close();
        }
        catch (Exception ex) {
            ex.printStackTrace();
            System.exit(0);
        }
    }

    public byte current() {
        if (this.eof())
            return 0;
        return _buffer[_pos];
    }

    public byte next() {
        ++_pos;
        if (_pos >= _nBytes) {
            try {
                _nBytes = _file.read(_buffer);
            }
            catch (Exception ex) {
                ex.printStackTrace();
                System.exit(0);
            }
            _pos = 0;
        }
        return _buffer[_pos];
    }

    public boolean eof() {
        return _nBytes <= 0;
    }

    public void printNBytes(int nBytes) {
        while (!this.eof() && nBytes > 0) {
            System.out.format("%x ", this.current());
            this.next();
            --nBytes;
        }
        System.out.println();
    }

}