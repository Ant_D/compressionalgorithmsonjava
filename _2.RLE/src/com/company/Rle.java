package com.company;

import java.io.*;

public class Rle {
    private final static int MAX_LEN = 128;

    private enum STATES {
        NONE,
        CHECK,
        REPEAT,
        UNIQUE
    }

    private static byte[] _buffer = new byte[MAX_LEN];

    private static void _write(BytesWriter writer, STATES state) {
        if (state == STATES.REPEAT) {
            _buffer[0] |= 1 << 7;
            writer.write(_buffer, 0, 2);
        }
        else if (state == STATES.UNIQUE) {
            writer.write(_buffer, 0, _buffer[0] + 1);
        }
    }

    public static void encode(InputStream istream, OutputStream ostream) {
        BytesReader reader = new BytesReader(istream);
        BytesWriter writer = new BytesWriter(ostream);
        STATES state = STATES.NONE;
        int cur = 0, prev = 0;

        while ((cur = reader.read()) != -1) {

            if (state == STATES.NONE) {
                state = STATES.CHECK;
                _buffer[0] = 0;
                _buffer[++_buffer[0]] = (byte)cur;
            }
            else if (state == STATES.CHECK) {
                if (cur == prev)
                    state = STATES.REPEAT;
                else
                    state = STATES.UNIQUE;
            }

            if (state == STATES.UNIQUE) {
                if (cur == prev) {
                    --_buffer[0];
                    _write(writer, state);
                    _buffer[0] = 2;
                    _buffer[1] = (byte)cur;
                    state = STATES.REPEAT;
                }
                else
                    _buffer[++_buffer[0]] = (byte)cur;
            }
            else if (state == STATES.REPEAT) {
                if (cur != prev) {
                    _write(writer, state);
                    _buffer[0] = 1;
                    _buffer[_buffer[0]] = (byte)cur;
                    state = STATES.CHECK;
                }
                else
                    ++_buffer[0];
            }

            prev = cur;
            if (_buffer[0] == MAX_LEN - 1) {
                _write(writer, state);
                state = STATES.NONE;
            }
        }

        _write(writer, state);

        writer.flush();
    }

    public static void decode(InputStream istream, OutputStream ostream) {
        BytesReader reader = new BytesReader(istream);
        BytesWriter writer = new BytesWriter(ostream);
        STATES state = STATES.NONE;
        int length = 0, c;

        while ((c = reader.read()) != -1) {
            if (state == STATES.NONE) {
                length = c;
                if ((length & 1 << 7) != 0)
                    state = STATES.REPEAT;
                else
                    state = STATES.UNIQUE;
                length &= (1 << 7) - 1;
            }
            else if (state == STATES.REPEAT)
                while (length-- > 0)
                    writer.write(c);
            else if (state == STATES.UNIQUE) {
                writer.write(c);
                --length;
            }

            if (length <= 0)
                state = STATES.NONE;
        }

        writer.flush();
    }
}