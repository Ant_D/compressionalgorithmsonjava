package com.company;

import org.apache.commons.cli.*;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        String ifname = null,
                ofname = null;
        boolean decompress = false;

        Options options = new Options();
        options.addOption("i", "input", true, "Input file");
        options.addOption("o", "output", true, "Output file");
        options.addOption("d", "decompress", false, "Decompress");
        options.addOption("h", "help", false, "Help");

        CommandLineParser parser = new DefaultParser();
        try {
            CommandLine cmd = parser.parse(options, args);
            Iterator<Option> it = cmd.iterator();
            HelpFormatter formatter = new HelpFormatter();

            while (it.hasNext()) {
                Option opt = it.next();
                switch (opt.getId()) {
                    case 'i':
                        ifname = opt.getValue();
                        break;
                    case 'o':
                        ofname = opt.getValue();
                        break;
                    case 'd':
                        decompress = true;
                        break;
                    case 'h':
                        formatter.printHelp("RLE", options);
                        break;
                    default:
                        System.out.println("Unhandled key " + (char)opt.getId());
                        System.out.println("Use key -h for help");
                }
            }

            if (ifname == null || ofname == null) {
                System.err.println("Provide input and output files");
                System.exit(0);
            }

            InputStream istream = new FileInputStream(ifname);
            OutputStream ostream = new FileOutputStream(ofname);

            if (decompress)
                Rle.decode(istream, ostream);
            else
                Rle.encode(istream, ostream);
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
    }
}
