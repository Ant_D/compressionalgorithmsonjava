package com.company;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collection;
import java.util.LinkedList;
import java.util.ListIterator;

public class MTF {
//    =================
//      PRIVATE
//    =================
    private static final int BUFFER_SIZE = 1024,
                             ALPHABET_SIZE = 256;

    private static void fillByAlphabet(Collection<Byte> col) {
        for (int i = Byte.MIN_VALUE; i <= Byte.MAX_VALUE; ++i)
            col.add((byte)i);
    }

//    =================
//      PUBLIC
//    =================

    public static void encode(InputStream istream, OutputStream ostream) {
        BytesReader br = new BytesReader(istream);
        BytesWriter bw = new BytesWriter(ostream);
        LinkedList<Byte> alphabet = new LinkedList<Byte>();
        byte[] buffer = new byte[BUFFER_SIZE],
                encoded = new byte[BUFFER_SIZE];
        int length;
        ListIterator<Byte> iterator;

        fillByAlphabet(alphabet);

        while ((length = br.read(buffer, 0, BUFFER_SIZE)) != -1) {
            for (int i = 0; i < length; ++i) {
                iterator = alphabet.listIterator();
                while (iterator.hasNext() && buffer[i] != iterator.next());

                assert iterator.previousIndex() < ALPHABET_SIZE;

                encoded[i] = (byte)iterator.previousIndex();
                iterator.remove();
                alphabet.addFirst(buffer[i]);
            }
            bw.write(encoded, 0, length);
        }

        bw.close();
    }

    public static void decode(InputStream istream, OutputStream ostream) {
        BytesReader br = new BytesReader(istream);
        BytesWriter bw = new BytesWriter(ostream);
        LinkedList<Byte> alphabet = new LinkedList<Byte>();
        byte[] buffer = new byte[BUFFER_SIZE],
                decoded = new byte[BUFFER_SIZE];
        int length, index;
        ListIterator<Byte> iterator;

        fillByAlphabet(alphabet);

        while ((length = br.read(buffer, 0, BUFFER_SIZE)) != -1) {
            for (int i = 0; i < length; ++i) {
                index = ((int)buffer[i] + 256) % 256;
                iterator = alphabet.listIterator(index);
                decoded[i] = iterator.next();
                iterator.remove();
                alphabet.addFirst(decoded[i]);
            }
            bw.write(decoded, 0, length);
        }

        bw.close();
    }
}
