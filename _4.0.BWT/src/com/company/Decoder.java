package com.company;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Comparator;

class IndexComparator implements Comparator<Integer> {
    private byte[] values;

    IndexComparator(byte[] values) {
        this.values = values;
    }

    private int sign(int x) {
        if (x < 0)
            return -1;
        if (x > 0)
            return 1;
        return 0;
    }

    @Override
    public int compare(Integer a, Integer b) {
        return sign(values[a] - values[b]);
    }
}

public class Decoder {
    private static void fillByIndexes(int length, Integer[] x) {
        for (int i = 0; i < length; ++i)
            x[i] = i;
    }

    private static void getPositionsForIndexes(int length, Integer[] sortedIndexes, int[] pos) {
        for (int i = 0; i < length; ++i)
            pos[sortedIndexes[i]] = i;
    }

    private static void getDecoded(int index, int length, byte[] buffer, int[] pos, byte[] decoded) {
        int i = index;
        for (int j = 0; j < length; ++j) {
            decoded[length - j - 1] = buffer[i];
            i = pos[i];
        }
    }

    private static int readIndex(BytesReader br) {
        int index = 0, part;
        for (int i = 0; i < 2; ++i) {
            part = br.read();
            if (part == -1)
                return -1;
            index = (index << 8) | part;
        }
        return index;
    }

    public static void decode(InputStream istream, OutputStream ostream) {
        BytesReader br = new BytesReader(istream);
        BytesWriter bw = new BytesWriter(ostream);

        int index, length;
        byte[] buffer = new byte[BWT.BUFFER_SIZE],
                decoded = new byte[BWT.BUFFER_SIZE];
        Integer[] indexes = new Integer[BWT.BUFFER_SIZE];
        int[] pos = new int[BWT.BUFFER_SIZE];

        while ((index = readIndex(br)) != -1 &&
                (length = br.read(buffer, 0, BWT.BUFFER_SIZE)) != -1)
        {
            fillByIndexes(length, indexes);
            Arrays.sort(indexes, 0, length, new IndexComparator(buffer));
            getPositionsForIndexes(length, indexes, pos);
            getDecoded(index, length, buffer, pos, decoded);
            bw.write(decoded, 0, length);
        }

        bw.close();
    }
}
