package com.company;

import java.io.InputStream;
import java.io.OutputStream;

public class BWT {
    static final int BUFFER_SIZE = 4096; // Максимум может быть 2^16

    public static void encode(InputStream istream, OutputStream ostream) {
        Encoder.encode(istream, ostream);
    }

    public static void decode(InputStream istream, OutputStream ostream) {
        Decoder.decode(istream, ostream);
    }
}
