package com.company;

import java.io.*;
import java.util.Arrays;
import java.util.Comparator;

class ArrayComparator implements Comparator<byte[]> {
    private int length;

    ArrayComparator(int length) {
        this.length = length;
    }

    private int sign(int x) {
        if (x > 0)
            return 1;
        if (x < 0)
            return -1;
        return 0;
    }

    @Override
    public int compare(byte[] a, byte[] b) {
        for (int i = 0; i < length; ++i)
            if (a[i] != b[i])
                return sign(a[i] - b[i]);
        return 0;
    }
}

public class Encoder {
    private static void getSortedCircularShifts(int length, byte[] buffer, byte[][] shifts) {
        for (int i = 0; i < length; ++i)
            for (int j = 0; j < length; ++j)
                shifts[i][j] = buffer[(i + j) % length];

        Arrays.sort(shifts, 0, length, new ArrayComparator(length));
    }

    private static void getLastColumn(int length, byte[][] shifts, byte[] lastColumn) {
        for (int i = 0; i < length; ++i)
            lastColumn[i] = shifts[i][length - 1];
    }

    public static void encode(InputStream istream, OutputStream ostream) {
        BytesReader br = new BytesReader(istream);
        BytesWriter bw = new BytesWriter(ostream);

        int length, index;
        byte b;
        byte[] buffer = new byte[BWT.BUFFER_SIZE],
                lastColumn = new byte[BWT.BUFFER_SIZE];
        byte[][] shifts = new byte[BWT.BUFFER_SIZE][BWT.BUFFER_SIZE];
        
        while ((length = br.read(buffer, 0, BWT.BUFFER_SIZE)) != -1) {
            getSortedCircularShifts(length, buffer, shifts);
            getLastColumn(length, shifts, lastColumn);
            index = Arrays.binarySearch(shifts, 0, length, buffer, new ArrayComparator(length));
            b = (byte)((index & 0xff00) >> 8);
            bw.write(b);
            b = (byte)(index & 0x00ff);
            bw.write(b);
            bw.write(lastColumn, 0, length);
        }

        bw.close();
    }

}
